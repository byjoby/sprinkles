# Sprinkles.js

Simple vanilla Javascript to enhance vanilla HTML/CSS experiences. Decent UX is possible without a teetering pile of complex build tools and Javascript libraries. Sprinkles is built from the assumption that simple is good and new is dangerous.